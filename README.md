# OS and Linux Intro Class

This class we'll look at OS systems and mostly Linux 

### OS

OS stands for Operating System. They are a level of abstractions on the bare metal.

Some have GUI (grafical user interface) such as Android, Microsoft, MacOS and lots of Linux distributions.

Others do not.

Some are paid, other are open source.

We will be using Linux a lot!

### Linux 

There are many linux distributions, here are some:

- ubuntu (user friendly, wildly used)
- Fedora
- Redhad distros
- Arch
- Kali (security testing and penetration testing / ethical hacking)

#### Bash and Shells

Linux comes with Bash terminals, and they might have different shells.

Different shell behave slightly different.

- bash
- oh-my-zsh
- gitbash
- other

**small difference in some commands**

#### Package managers

performed through the package manager

-RHEL = yum, dnf or rpm
- Debian = apt-get, aptitude or dpkg

These help install software - Such as python or nginx and apache

#### Aspects

Everything is a file
- everything can interact with everything - this is great for automation.

#### 3 file desxfiptiors

To interact with programs and with files. We can use, redirect and play around with 0, 1 and 2 that are the stdin, stdout and stderr

**0 is Standard input (stdin)**
**1 is Standard output *STDOUT)**
**2 is Standard error (stderr)**

stdin, stdout, and stderr are three data streams created when you launch a Linux command. You can use them to tell if your scripts are being piped or redirected.

### Two important hard paths `/`and `~`

`/` - means root directory
`~` - means `/users/your_login_user/`

### Environment variables

Environment is a place where code runs.
In bash we might want to set some variables.

Variables is like a box, you have a name and you add stuff to it. yOU CAN OPEN IT LATER.

In bash we define a variable simply by:

```bash

# Setting a variable
MY_VAR="This is a VAR in my code"

# call the variable using echo
echo $MY_VAR
>"This is a VAR in my code"

# You can re assign the variable 
MY_VAR="Hello"
echo $MY_VAR
> hello

# Call other variables the same way 
echo $USER
> Shannon

```
What happens if I close my terminal or open a new one?

Once you close your terminal session, all variable not in the path will be lost.

#### Child process

A process that is iniciated under another process like running another bash script.

It goes via the $PATH but is a new "terminal" essentially.

Hence if we:

```bash
#1) set variable in terminal
LANDLORD="Jess"

# run the bash script that has echo $LANDLORD
./bash_file.sh
> hi from the file
>
>Landlord above^^^
```

This happens because the ./bash_file.sh runs as a child process - A new terminal.


To make a Variable available to a child process, you need to ```export```

```$ export <VARIABLE> ``` 

#### setting and defining environment variable

You need to add them to a file on the path and export them.

### Path

Terminal & bash process follow the PATH

There is a PATH for login users that have a profile and the ones without.

Files to consider to type in variables:

```bash
# these always exist at location
> .zprofile
>.zshrc

# for bash shell without oh-my-zsh:
> .bashrc
> .bash_profile
> .bashprofile
```


### Common Commands

```

# Give file permission
$ chmod +rwx <files>
```
### Permission

### Users and Sudoes



### Wild Cards

Wildcards (or meta characters) are symbols or special characters that represent other characters. You can use them with any command such as ls command or rm commands to list or remove files matching a given criteria.

*example*
![wildcard](./images/wildcard_demo.png)


### Matchers

You can use this to help you match files or content in files.

```bash
# any number of characters to a side 
ls exam*
> example    example.jpg example.txt example2

ls *exam
> this_is_an_exam

# ? for specific number of characters
ls example????
>example.jpg example.txt

## List of specific characters
# each one of these represents 1 character.
ls example.[aeiout][a-z][a-z]
>example.txt
```
### Redirects

Every command has 3 defaults.

- stdin - Represents 0
- stdout - Represent 1
- stderr - Represent 2

example of stdout
```bash
ls
> README.md  example  example.txt
notes (...)
# the list that prints is the stdout!
```

example giving `ls` a stdin - the example????

```bash
# the example???? is the stdin
ls example????
> example.jpg example.txt
# the output is stdout
```

if there is an error, you get a stderr:
```bash
ls example???
> zsh: no matches found: example???
# the above is a stderr
```

The cool thing is you can redirect this!

*example*

![redirecting1](./images/redirecting_1.png)

...

![redirecting2](./images/redirecting_2.png)

...

![redirecting3](./images/redirecting_3.png)

#### How to redirect to the same file

$ ls -l | grep exam > out 2>&1

### Pipes

A pipe is a form of redirection (tranfsfer of standard output to some other destination) that is used in Linux and other Unix-like operating systems to send the output of one command/program/process to another comand/program/process for further processing.

*example*
![pipe](./images/piping.png)



The above command is listing all visible directories in /etc that are accessible by the world. I have none so the output is `0`

#### Grep

grep is a command-line utility for searching plain-text data sets for lines that match a regular expression.

*example*
![grep](./images/grep.png)



